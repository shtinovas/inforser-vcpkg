set(VCPKG_POLICY_EMPTY_INCLUDE_FOLDER enabled)

vcpkg_download_distfile(ARCHIVE
    URLS "https://nexus.cqms.ru/repository/inforser-raw/hw-service-dependencies/zebra.tar.gz"
    FILENAME "zebra.tar.gz"
    SHA512 f5def8af78c153f2296922e2659a6ab7910e1ebc42c21af849323533b14ea01335447e74b87436e38329eb3f3661a12d72398479d44a18a9ad5120e4e98de743
)

vcpkg_extract_source_archive(
    EXTRACTED
    ARCHIVE "${ARCHIVE}"
)

file(
    COPY
        ${EXTRACTED}/include
        ${EXTRACTED}/lib
    DESTINATION
        ${CURRENT_INSTALLED_DIR}/${PORT}
)

file(
    COPY
        ${EXTRACTED}/copyright
    DESTINATION
        ${CURRENT_PACKAGES_DIR}/share/${PORT}
)